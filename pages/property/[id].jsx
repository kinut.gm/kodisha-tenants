import {
  ArrowRightOutlined,
  FlagOutlined,
  HeartOutlined,
  LikeOutlined,
  PhoneOutlined,
  SaveOutlined,
} from "@ant-design/icons"
import { Carousel, Row, Col, Tag, Button } from "antd"

export default function Property() {
  const content = {
    background: "#f1f1f1",
    height: 200,
    width: "90%",
  }

  return (
    <div style={{ padding: 24, width: "100%" }}>
      <Carousel autoplay style={{ height: 200, width: "100%" }}>
        <div>1</div>
        <div>2</div>
        <div>3</div>
        <div>4</div>
      </Carousel>

      {/* Details */}
      <div id="name" style={{ marginBottom: "2rem" }}>
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Property name</p>
        <div style={{ display: "flex" }}>
          <div style={{ width: "90%" }}>
            <h3
              style={{
                color: "#808080",
                fontWeight: "800",
                fontSize: "1.3rem",
                margin: 0,
              }}
            >
              Sunton Heights
            </h3>
            <p style={{ color: "#808080", fontSize: "0.9rem" }}>
              Buiding description goes here
            </p>
          </div>
          <Button
            type="secondary"
            style={{
              height: 45,
              width: 40,
              border: "none",
              outline: "none",
              boxShadow: "none",
            }}
          >
            <HeartOutlined />
          </Button>
        </div>
      </div>

      <div id="units" style={{ marginBottom: "2rem" }}>
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Units layout</p>
        {[1, 1, 1].map((el) => (
          <Row key={el}>
            <Col span={10}>
              <p>Bedsitters</p>
            </Col>
            <Col span={6}>
              <p>~12sq m</p>
            </Col>
            <Col span={8}>
              <p>Ksh 7,500</p>
            </Col>
          </Row>
        ))}
      </div>

      <div id="vacancy" style={{ marginBottom: "2rem" }}>
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Vacancy</p>
        {[1, 1, 1].map((el) => (
          <Row key={el}>
            <p style={{ marginRight: 12 }}>Bedsitters</p>
            <Tag color="#9100FF" style={{ height: 24, borderRadius: 8 }}>
              <p style={{ fontWeight: 700 }}>2</p>
            </Tag>
          </Row>
        ))}
      </div>

      <div id="ammenities" style={{ marginBottom: "2rem" }}>
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Ammenities</p>
        <div style={{ display: "flex", flexWrap: "wrap" }}>
          {[1, 1, 1, 1].map((el) => (
            <Tag color="green" style={{ marginBottom: 8 }} key={el}>
              24/7 security
            </Tag>
          ))}
        </div>
      </div>

      <div id="caretaker" style={{ marginBottom: "2rem" }}>
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Caretaker</p>
        <span style={{ width: "100%" }}>
          <h3 style={{ display: "inline" }}>John</h3>
          <Button
            type="secondary"
            style={{
              height: 35,
              marginLeft: 24,
              outline: "none",
              boxShadow: "none",
            }}
          >
            <PhoneOutlined />
          </Button>
        </span>
      </div>

      <div id="mapview">
        <p style={{ color: "#9100FF", fontSize: "0.7rem" }}>Location</p>
        <div
          style={{
            background: "#f1f1f1",
            width: "100%",
            height: 300,
            position: "relative",
          }}
        >
          <Button
            style={{
              background: "#9100FF",
              position: "absolute",
              right: "1rem",
              bottom: "1rem",
            }}
            type="primary"
          >
            Go <ArrowRightOutlined style={{ marginLeft: 12 }} />
          </Button>
        </div>
      </div>
    </div>
  )
}
