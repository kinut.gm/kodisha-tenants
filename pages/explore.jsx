import { FilterOutlined, MenuOutlined } from "@ant-design/icons";
import { Input, Button } from "antd";

export default function Explore() {
  return (
    <div style={{ padding: 24, width: "100%" }}>
      <div
        style={{
          display: "flex",
          marginBottom: "1.7rem",
        }}
      >
        <Input
          placeholder="Search building or location"
          style={{
            padding: 12,
            background: "#f1f1f1",
            border: "none",
            marginRight: 4,
          }}
        />
        <Button
          type="secondary"
          style={{
            height: 45,
            width: 40,
            border: "none",
            outline: "none",
            boxShadow: "none",
          }}
        >
          <FilterOutlined />
        </Button>
        <Button
          type="secondary"
          style={{
            height: 45,
            width: 40,
            border: "none",
            outline: "none",
            boxShadow: "none",
          }}
        >
          <MenuOutlined />
        </Button>
      </div>
      <div>
        <h3
          style={{
            color: "#9100FF",
            marginBottom: "1.5rem",
            fontSize: "1.2rem",
            fontWeight: "bold",
            letterSpacing: "-0.03rem",
          }}
        >
          Based on your location
        </h3>
      </div>
    </div>
  );
}
