import {
  FacebookFilled,
  ArrowRightOutlined,
  InstagramOutlined,
  TwitterOutlined,
  WhatsAppOutlined,
} from "@ant-design/icons";
import { Button, Divider } from "antd";

export default function Contacts() {
  return (
    <div style={{ padding: 24 }}>
      <h3
        style={{
          color: "#9100FF",
          marginBottom: "1rem",
          fontSize: "1.5rem",
          fontWeight: "bolder",
          letterSpacing: "-0.03rem",
        }}
      >
        Reach us
      </h3>

      <div style={{ padding: 24 }}>
        {/* Socials */}
        <p>Find us through our socials</p>
        <div
          style={{
            padding: "24px 0px",
            display: "flex block",
            margin: "0 auto",
          }}
        >
          <Button
            type="secondary"
            style={{
              border: "none",
              outline: "none",
              boxShadow: "none",
            }}
          >
            <WhatsAppOutlined style={{ fontSize: "2rem" }} />
          </Button>
          <Button
            type="secondary"
            style={{
              border: "none",
              outline: "none",
              boxShadow: "none",
            }}
          >
            <InstagramOutlined style={{ fontSize: "2rem" }} />
          </Button>
          <Button
            type="secondary"
            style={{
              border: "none",
              outline: "none",
              boxShadow: "none",
            }}
          >
            <FacebookFilled style={{ fontSize: "2rem" }} />
          </Button>
          <Button
            type="secondary"
            style={{
              border: "none",
              outline: "none",
              boxShadow: "none",
            }}
          >
            <TwitterOutlined style={{ fontSize: "2rem" }} />
          </Button>
        </div>

        {/* Email */}
        <Divider orientation="center">or</Divider>
        <p>Send us an email</p>

        <Button
          style={{
            background: "#9100FF",
            position: "absolute",
            right: "2rem",
            bottom: "2rem",
          }}
          type="primary"
        >
          Send <ArrowRightOutlined style={{ marginLeft: 12 }} />
        </Button>
      </div>
    </div>
  );
}
