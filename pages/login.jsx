import { Button, Divider } from "antd"
import { ArrowRightOutlined } from "@ant-design/icons"

export default function Login() {
  return (
    <div style={{ padding: 48 }}>
      <div
        style={{
          position: "absolute",
          top: "20%",
          width: "calc(100% - 96px)",
          background: "yellow",
        }}
      >
        <p>It&apos;s time we knew who you are</p>

        <Button
          style={{
            background: "#9100FF",
            float: "right",
            display: "block",
            marginBottom: "3rem",
          }}
          type="primary"
        >
          Next <ArrowRightOutlined style={{ marginLeft: 12 }} />
        </Button>

        <Divider orientation="center">or</Divider>

        <Button type="primary" style={{ width: "100%", height: 45 }}>
          Sign in with Google
        </Button>
      </div>
    </div>
  )
}
