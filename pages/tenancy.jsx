import { Button, Empty } from "antd";
import { useState } from "react";

export default function Tenanacy() {
  const [empty, setEmpty] = useState(true);

  return (
    <div style={{ padding: 24 }}>
      <h3
        style={{
          color: "#9100FF",
          marginBottom: "1rem",
          fontSize: "1.5rem",
          fontWeight: "bolder",
          letterSpacing: "-0.03rem",
        }}
      >
        Tenancy
      </h3>
      <div
        style={{
          width: "100%",
          height: "calc(100vh - 100px)",
          overflow: "scroll",
        }}
      >
        {empty && <Empty />}
        <Button
          type="secondary"
          style={{
            width: "80%",
            margin: "0 auto",
            color: "#9100FF",
            background: "transparent",
            border: "#9100FF 1px solid",
            display: "block",
            marginTop: "5rem",
          }}
        >
          Explore other rentals
        </Button>
      </div>
    </div>
  );
}
